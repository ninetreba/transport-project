package com.accenture.atc.transport;

import com.accenture.atc.Parking;

public class ElectricScooter extends Transport {
    private int battery;
    private int maxSpeed;

    public ElectricScooter(int id, Type type, Condition condition, Parking currentParking, int battery, int maxSpeed) {
        super(id, type, condition, currentParking);
        this.battery = battery;
        this.maxSpeed = maxSpeed;
    }

}