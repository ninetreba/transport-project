package com.accenture.atc.transport;

import com.accenture.atc.Parking;

public class Bicycle extends Transport {

    public Bicycle(int id, Type type, Condition condition, Parking currentParking) {
        super(id, type, condition, currentParking);
    }
}
