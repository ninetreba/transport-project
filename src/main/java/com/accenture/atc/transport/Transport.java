package com.accenture.atc.transport;

import com.accenture.atc.Parking;

public abstract class Transport {
    protected int id;
    protected Type type;
    protected Condition condition;
    protected Parking currentParking;

    public Transport(int id, Type type, Condition condition, Parking currentParking) {
        this.id = id;
        this.type = type;
        this.condition = condition;
        this.currentParking = currentParking;
    }
}