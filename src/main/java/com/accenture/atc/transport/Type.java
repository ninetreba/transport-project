package com.accenture.atc.transport;

public enum Type {
    BICYCLE, ELECTRIC_SCOOTER;
}
