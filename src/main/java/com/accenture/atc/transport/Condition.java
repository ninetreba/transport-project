package com.accenture.atc.transport;

public enum Condition {
    EXCELLENT, GOOD, SATISFACTORY;
}
