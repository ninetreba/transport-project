package com.accenture.atc;

public enum ParkingType {
    ALL, BICYCLE_ONLY, ELECTRIC_SCOOTER_ONLY;
}
