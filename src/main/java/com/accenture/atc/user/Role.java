package com.accenture.atc.user;

public enum Role {
    ADMIN, USER;
}
