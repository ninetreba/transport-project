package com.accenture.atc.user;

public abstract class User {
    private String login;
    private String password;
    private Role role;


    public User(String login, String password, Role role) {
        this.login = login;
        this.password = password;
        this.role = role;
    }


}
