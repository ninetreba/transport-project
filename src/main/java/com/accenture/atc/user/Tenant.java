package com.accenture.atc.user;

import java.math.BigDecimal;

public class Tenant extends User {
    private BigDecimal balance;

    public Tenant(String login, String password, Role role, BigDecimal balance) {
        super(login, password, role);
        this.balance = balance;
    }

}
