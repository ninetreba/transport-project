package com.accenture.atc;

import com.accenture.atc.transport.Transport;

import java.util.List;

public class Parking {
    private int id;
    private int coordinateX;
    private int coordinateY;
    private int radius;
    private ParkingType parkingType;

    private static List<Transport> transports;

    public Parking(int id, int coordinateX, int coordinateY, int radius, ParkingType parkingType) {
        this.id = id;
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.radius = radius;
        this.parkingType = parkingType;
    }

    public void searchFreeTransport() {
        // тип метода void - это временно
    }
}
